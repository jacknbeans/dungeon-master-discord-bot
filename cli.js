#!/usr/bin/env node

"use strict"

const Discord = require('discord.js');
var admin = require('firebase-admin');

var serviceAccount = require('./firebase-key.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://dungeon-master-65ab5.firebaseio.com'
});

var db = admin.firestore();

var dungeonsDb = db.collection('dungeons');
var rulesDb = db.collection('rules');

var rulesTemplate = {};
var guildRules = {};

const ruleValueTypes = [
  'bool',
  'role',
  'textChannel'
];

const ruleValueTypeIndices = {
  bool: 0,
  role: 1,
  textChannel: 2
}

// These names are used as keys in the database. CHANGE AT RISK OF BREAKING DATABASE! Either that or make code to make it backwards compatible (I'm too lazy right now)
const shameInDungeonRuleName = 'shameInDungeon';
const adminCancelRuleName = 'adminCancel';
const errorChannelRuleName = 'errorChannel';
const adminRoleRuleName = 'adminRole';
const adminSetRuleRuleName = 'adminSetRule';
const adminSetDungeonRuleName = 'adminSetDungeon';

const rules = [
  {
    name: adminRoleRuleName,
    description: 'Which role should be used to verify admin status of a user executing certain commands.\nType: string\nValid Value: The _exact_ name of the role as it is displayed.',
    defaultValue: '',
    valueType: ruleValueTypes[ruleValueTypeIndices.role]
  },
  {
    name: errorChannelRuleName,
    description: 'Which channel should be used to display any error messages that the Dungeon Master may encounter.\nType: string\nValid Value: The _exact_ name of a text channel.',
    defaultValue: '',
    valueType: ruleValueTypes[ruleValueTypeIndices.textChannel]
  },
  {
    name: shameInDungeonRuleName,
    description: 'Whether or not a user in the dungeon can initiate a !shame vote.\n\Type: bool\nValid Value: true|false',
    defaultValue: true,
    valueType: ruleValueTypes[ruleValueTypeIndices.bool]
  },
  {
    name: adminCancelRuleName,
    description: 'Whether or not the adminRole is allowed to cancel an initiated !shame vote.\nType: bool\nValid Value: true|false',
    defaultValue: false,
    valueType: ruleValueTypes[ruleValueTypeIndices.bool]
  },
  {
    name: adminSetRuleRuleName,
    description: `Whether or not a user is required to have the \`${adminRoleRuleName}\` in order to set rules.\nType: bool\nValid Value: true|false`,
    defaultValue: false,
    valueType: ruleValueTypes[ruleValueTypeIndices.bool]
  },
  {
    name: adminSetDungeonRuleName,
    description: `Whether or not a user is required to have the \`${adminRoleRuleName}\` in order to set the dungeon.\nType: bool\nValid Value: true|false`,
    defaultValue: false,
    valueType: ruleValueTypes[ruleValueTypeIndices.bool]
  }
];

// Setup the rulesTemplate
rules.forEach(rule => {
  rulesTemplate[rule.name] = rule.defaultValue;
});

// Setup the rulesEmbedFields
var rulesEmbedFields = rules.map(function(rule) {
  return {
    name: rule.name,
    value: rule.description
  };
});

var errorChannel = {};

const client = new Discord.Client();
const secondsToVoteDefault = 15;
const shameTimerInterval = 5;
const shameTimeDefault = 5;

var dungeons = {};
var voiceConnections = {};

var shameAuthor = {};
var shameMember = {};
var shameMessage = {};
var shameTime = {};
var shameTimeStr = {};
var secondsToVote = {};

var shameMessageTimer = {};
var shameMessageContent = {};

var shamedMembers = {};

var MapParamFields = function(parameter) {
  return {
    name: parameter.name,
    value: parameter.description
  };
};

const helpCommand = {
  name: '!help',
  generalDescription: 'Show extra details on a specific command (<command-name> does not need to include the \'!\').',
  parameters: [
    {
      name: '<command-name>'
    }
  ]
};
const helpInfo = {
  name: helpCommand.name,
  generalDescription: 'Displays this help information.'
}

const shameCommand = {
  name: '!shame',
  generalDescription: 'Used to start a vote to see whether or not a user should be put in the dungeon.',
  detailedDescription: `Initiates a vote that people will be able to vote yea (👍) or nay (👎) on. Depending on the outcome, the person will be put into the dungeon for a certain period of time. If the rule \`${shameInDungeonRuleName}\` is set to \`true\` then those who are in the dungeon won't be allowed to initiate a vote.`,
  parameters: [
    {
      name: '<user-mention>',
      description: 'Mention a user (@user) that you\'d like to see put in the dungeon.'
    },
    {
      name: '<optional-time-to-shame>',
      description: 'The amount of time that you\'d like to see the mentioned user be in the dungeon. This can either be just a number representing the number of seconds or you can format it in MM:SS if that\'s easier. The maximum of this time is 1:30 (90 seconds) and the minimum is 0:01 (1 second).'
    },
    {
      name: '<optional-time-to-vote>',
      description: 'The amount of time that you\'d like to see the vote last for. This can either be just a number representing the number of seconds or you can format it in MM:SS if that\'s easier. The maximum of this time is 1:30 (90 seconds) and the minimum is 0:05 (5 seconds).'
    }
  ]
};
const shameCommandParamFields = shameCommand.parameters.map(MapParamFields);

const setDungeonCommand = {
  name: '!setDungeon',
  generalDescription: 'Sets which channel will be used as the dungeon.',
  detailedDescription: `Sets which channel will be used as the dungeon. If the rule\`${adminSetDungeonRuleName}\` is set to \`true\` then only users with the \`${adminRoleRuleName}\` will be allowed to set the dungeon.`,
  parameters: [
    {
      name: '<channel-name>',
      description: 'This should be the _exact_ name of the channel (no surrounding quotes) and it should also be a voice channel, otherwise this command will fail.'
    }
  ]
};
const setDungeonCommandParamFields = setDungeonCommand.parameters.map(MapParamFields);

const setRuleCommandName = '!setRule';
const listRules = {
  name: `${helpCommand.name} rules`,
  generalDescription: `List all rules that can be modified by ${setRuleCommandName}.`,
  detailedDescription: `Here is a complete list of all rules and their associated value types that you can use with ${setRuleCommandName}:`
};
const setRuleCommand = {
  name: setRuleCommandName,
  generalDescription: 'Used to setup some custom rules regarding these commands.',
  detailedDescription: `Set a rule for the Dungeon Master to follow or use. These are predefined and if you want a rule added, let it be known! For a complete list of all rules and there value types, use ${helpCommand.name} ${listRules.name}. The rule \`${adminSetRuleRuleName}\`, when set to true, will only allow users with the \`${adminRoleRuleName}\` to set the rules.`,
  parameters: [
    {
      name: '<rule>',
      description: 'Which rule you want to set. This must be the _exact_ name of the rule, or else this won\'t work.'
    },
    {
      name: '<value>',
      description: 'The new value that you\'d like to set the rule to.'
    }
  ]
};
const setRuleParamFields = setRuleCommand.parameters.map(MapParamFields);

const cancelCommand = {
  name: '!cancel',
  generalDescription: 'Cancel the current vote.',
  detailedDescription: `Cancel the current vote that you've initiated. When the \`${adminCancelRuleName}\` rule is set to \`true\` it will allow any user with the \`${adminRoleRuleName}\` role to cancel an initiated vote.`
};

const rulesCommand = {
  name: '!rules',
  generalDescription: `List all the rules and their currently set values.`,
  detailedDescription: `Here's a complete list of all rules that can be used with ${setRuleCommand.name} and their currently set values:`
};

const shameHelpEmbed = {
  title: `${shameCommand.name} ${shameCommand.parameters[0].name} ${shameCommand.parameters[1].name} ${shameCommand.parameters[2].name}`,
  description: shameCommand.detailedDescription,
  color: 1957120,
  fields: shameCommandParamFields
};

const shameHelpFirstArgumentEmbed = {
  title: `${shameCommand.name} ${shameCommand.parameters[0].name}`,
  color: 1957120,
  fields: [
    shameCommandParamFields[0]
  ]
};

const shameHelpSecondArgumentEmbed = {
  title: `${shameCommand.name} ${shameCommand.parameters[0].name} ${shameCommand.parameters[1].name}`,
  color: 1957120,
  fields: [
    shameCommandParamFields[1]
  ]
};

const shameHelpThirdArgumentEmbed = {
  title: `${shameCommand.name} ${shameCommand.parameters[0].name} ${shameCommand.parameters[1].name} ${shameCommand.parameters[2].name}`,
  color: 1957120,
  fields: [
    shameCommandParamFields[2]
  ]
}

const setDungeonHelpEmbed = {
  title: `${setDungeonCommand.name} ${setDungeonCommand.parameters[0].name}`,
  description: setDungeonCommand.detailedDescription,
  color: 1957120,
  fields: setDungeonCommandParamFields
};

const setRuleHelpEmbed = {
  title: `${setRuleCommand.name} ${setRuleCommand.parameters[0].name}:${setRuleCommand.parameters[1].name}`,
  description: setRuleCommand.detailedDescription,
  color: 1957120,
  fields: setRuleParamFields
};

const listRulesHelpEmbed = {
  title: `${helpCommand.name} ${listRules.name}`,
  description: listRules.detailedDescription,
  color: 1957120,
  fields: rulesEmbedFields
};

const cancelHelpEmbed = {
  title: cancelCommand.name,
  description: cancelCommand.detailedDescription,
  color: 195712
};

const rulesHelpEmbed = {
  title: rulesCommand.name,
  description: rulesCommand.generalDescription,
  color: 195712
};
const rulesEmbed = {
  title: rulesCommand.name,
  description: rulesCommand.detailedDescription,
  color: 195712
};

const commands = [
  helpInfo,
  helpCommand,
  shameCommand,
  setDungeonCommand,
  setRuleCommand,
  listRules,
  cancelCommand,
  rulesCommand
]

const generalHelpFields = commands.map(function(command) {
  var commandName = command.name;
  if (command.parameters) {
    command.parameters.forEach(param => {
      commandName += ` ${param.name}`
    });
  }
  return {
    name: commandName,
    value: command.generalDescription
  }
});

const generalHelpEmbed = {
  title: 'Commands',
  color: 1957120,
  fields: generalHelpFields
};

var OnReady = async function() {
  try {
    var rulesSnapshot = await rulesDb.get();

    console.log('Updating rules from database...')

    rulesSnapshot.forEach(doc => {
      console.log(`Found rules for guild ${doc.id} in database`);

      guildRules[doc.id] = doc.data();

      // Make sure that any newly added rules are added to the guild rules and database
      var changed = false;
      rules.forEach(rule => {
        if (!guildRules[doc.id].hasOwnProperty(rule.name)) {
          guildRules[doc.id][rule.name] = rule.defaultValue;
          changed = true;
        }
      });

      if (changed) {
        rulesDb.doc(doc.id).set(guildRules[doc.id]);
      }
    });

    client.guilds.forEach(guild => {
      if (!guildRules[guild.id]) {
        console.log(`Setting up new rules for guild ${guild.id}(${guild})`);

        guildRules[guild.id] = rulesTemplate;
        rulesDb.doc(guild.id).set(guildRules[guild.id]);
      }
      else {
        if (guildRules[guild.id].errorChannel.length > 0) {
          var channel = guild.channels.get(guildRules[guild.id].errorChannel);
          if (channel) {
            console.log(`Found error channel ${guildRules[guild.id].errorChannel}(${channel.name}) for guild ${guild.id}(${guild})`);

            errorChannel[guild.id] = channel;
          }
          else {
            console.log(`Could not find the error channel for ${guild}. Was the channel deleted?`);
          }
        }
      }
    });
    
    var dungeonsSnapshot = await dungeonsDb.get();

    console.log('Updating dungeons from database...');

    var DungeonsSnapshotForEach = async function(doc) {
      console.log(`Found dungeon ${doc.data().id} for guild ${doc.id}`);

      console.log(`Validating dungeon...`);

      var guild = client.guilds.get(doc.id);
      if (guild) {
        var dungeon = guild.channels.get(doc.data().id);
        if (dungeon) {
          console.log(`Dungeon ${dungeon.id}(${dungeon.name}) is valid for guild ${guild.id}(${guild})`);

          dungeons[guild.id] = dungeon;
          try {
            voiceConnections[guild.id] = await dungeon.join();
            console.log(`I've entered ${dungeon.name} for ${guild}`);
          }
          catch(err) {
            if (errorChannel[guild.id]) {
              errorChannel[guild.id].send({
                embed: {
                  title: `Error: Could not connect to dungeon ${dungeon.name}!`,
                  description: `An error has occurred when trying to join ${dungeon.name}.`,
                  color: 14483459,
                  fields: [
                    {
                      name: 'Reason:',
                      value: err.toString()
                    }
                  ]
                }
              }).catch(console.error);
            }
            
            console.error(err);
          }
        }
        else {
          if (errorChannel[guild.id]) {
            errorChannel[guild.id].send([
              `Could not find the dungeon that was previously setup for this server. Was the channel deleted?`
            ]);
          }
        }
      }
      else {
        console.log(`There is a guild in the database that the bot is no longer a part of. Might want to tidy this up at some point...`);
      }
    };

    dungeonsSnapshot.forEach(doc => {
      DungeonsSnapshotForEach(doc);
    });
  }
  catch (err) {
    console.error(err);
  }
};

client.on('ready', () => {
  OnReady();
});

client.on('guildCreate', guild => {
  console.log(`Joined guild ${guild}!`);

  if (!guildRules[guild.id]) {
    console.log(`Setting up guild rules...`);
    guildRules[guild.id] = rulesTemplate;
  }
});

client.on('guildDelete', guild => {
  console.log(`Removed from guild ${guild}`);
});

var ResetShameInfo = function(guildId) {
  delete shameAuthor[guildId];
  delete shameMember[guildId];
  delete shameMessage[guildId];
  delete shameTime[guildId];
  delete shameTimeStr[guildId];
  delete secondsToVote[guildId];
  if (shameMessageTimer[guildId]) {
    clearInterval(shameMessageTimer[guildId]);
  }
  delete shameMessageTimer[guildId];
  delete shameMessageContent[guildId];
};

var ShameCheck = function(guildId, id) {
  if (shamedMembers[guildId][id]) {
    clearInterval(shamedMembers[guildId][id].timerHandle);

    var member = dungeons[guildId].guild.member(shamedMembers[guildId][id].id);
    if (member) {
      member.setVoiceChannel(shamedMembers[guildId][id].originalVoiceChannelID)
        .catch(console.error);

      var channel = dungeons[guildId].guild.channels.get(shamedMembers[guildId][id].messageChannelID);
      if (channel) {
        channel.send(`${member.displayName} has served their time in the ${dungeons[guildId]} and has been released. Next time, I won't be so easy on you...`);
      }

      delete shamedMembers[guildId][id];
    }
  }
};

var ShameFinalize = function(guildId) {
  if (shameMessage[guildId]) {
    const thumbsUp = '👍';
    const thumbsDown = '👎';

    var numVotesUp = 0;
    var numVotesDown = 0;
    
    shameMessage[guildId].reactions.forEach(reaction => {
      if (reaction.emoji.toString() === thumbsUp) {
        numVotesUp = reaction.count;
      }
      else if (reaction.emoji.toString() === thumbsDown) {
        numVotesDown = reaction.count;
      }
    });

    if (numVotesUp > numVotesDown) {
      shameMessage[guildId].channel.send([
        `${shameMember[guildId].displayName}, you have been found wanting and will be punished for ${shameTimeStr[guildId]} in the ${dungeons[guildId]}!`
      ]);

      shameMember[guildId].setVoiceChannel(dungeons[guildId])
        .catch(console.error);

      var timerHandle = setInterval(ShameCheck, 1000 * shameTime[guildId], guildId, shameMember[guildId].id);

      var shamedMemberData = {};
      shamedMemberData.name = shameMember[guildId].displayName;
      shamedMemberData.id = shameMember[guildId].id;
      shamedMemberData.timerHandle = timerHandle;
      shamedMemberData.originalVoiceChannelID = shameMember[guildId].voiceChannelID;
      shamedMemberData.messageChannelID = shameMessage[guildId].channel.id;

      shamedMembers[guildId] = {};
      shamedMembers[guildId][shameMember[guildId].id] = shamedMemberData;

      if (voiceConnections[guildId]) {
        voiceConnections[guildId].playFile('./shame.mp3');
      }
    }
    else if (numVotesUp < numVotesDown) {
      shameMessage[guildId].channel.send([
        `${shameMember[guildId].displayName}, the vote was with you this time and you will not be put in the ${dungeons[guildId]}. I'll be waiting, hope to see you soon...`
      ]);
    }
    else {
      shameMessage[guildId].channel.send([
        `${shameMember[guildId].displayName}, you might be lucky or they might actually like you, either way, the vote was even and you've escaped the ${dungeons[guildId]} this time...`
      ]);
    }

    ResetShameInfo(guildId);
  }
};

var ShameMessageUpdate = function(guildId) {
  if (shameMessage[guildId])
  {
    secondsToVote[guildId] -= shameTimerInterval;
    if (secondsToVote[guildId] <= 0) {
      clearInterval(shameMessageTimer[guildId]);
      shameMessageContent[guildId][1] = `Time's up!`;
      shameMessage[guildId].edit(shameMessageContent[guildId]);
      ShameFinalize(guildId);
    }
    else {
      shameMessageContent[guildId][1] = `${secondsToVote[guildId]} seconds left to vote!`;
      shameMessage[guildId].edit(shameMessageContent[guildId]);
    }
  }
};

var Shame = function(message) {
  if (message.content.trimRight().length > shameCommand.name.length) {
    const guildId = message.guild.id;

    if (!dungeons[guildId]) {
      message.channel.send(
        `There is no dungeon for me to control on ${message.guild}! Use the ${setDungeonCommand.name}:`,
        {embed: setDungeonHelpEmbed}
      );
      ResetShameInfo(message.guild.id);
      return;
    }

    if (guildRules[guildId][shameInDungeonRuleName] === false && shamedMembers[guildId] && shamedMembers[guildId][message.guild.member(message.author).id]) {
      message.channel.send([
        `${message.guild.member(message.author).displayName}, you are not allowed to shame someone while being in the dungeon!`
      ]);
      ResetShameInfo(message.guild.id);
      return;
    }
    
    if (shameMessage[guildId]) {
      message.channel.send([
        `There can only be one active shame vote at a time!`,
        `${shameAuthor[guildId].displayName} has already started a vote to put ${shameMember[guildId].displayName} in the ${dungeons[guildId]}!`,
        `Please wait till that vote has completed and try again.`
      ]);
      return;
    }

    
    shameAuthor[guildId] = message.guild.member(message.author);
    const user = message.mentions.users.first();
    
    if (user) {
      var member = message.guild.member(user);
      if (shamedMembers[guildId] && shamedMembers[guildId][member.id]) {
        message.channel.send([
          `${member.displayName} is already in the dungeon. ${shameAuthor[guildId]}, please wait till they're freed and try again.`
        ]);
        ResetShameInfo(guildId);
        return;
      }
      
      if (member) {
        shameMember[guildId] = member;
        if (!shameMember[guildId].voiceChannel) {
          message.channel.send(`${shameMember[guildId].displayName} is not in a voice channel and so the shaming will not work!`);
          ResetShameInfo(message.guild.id);
          return;
        }

        var parameters = message.content.replace(`${shameCommand.name} `, '');
        parameters = parameters.split(/\ +/g);

        if (parameters) {
          if (parameters.length > 1) {
            const shameTimeArg = parameters[1];
            
            if (!shameTimeArg.includes(':')) {
              var parsed = parseInt(shameTimeArg, 10);
              
              if (isNaN(parsed)) {
                message.channel.send(
                  `The argument '${shameTimeArg}' for ${shameCommand.parameters[1].name} could not be parsed as a number.`,
                  {embed: shameHelpSecondArgumentEmbed}
                );
                ResetShameInfo(message.guild.id);
                return;
              }

              const maxSec = 90;
              const minSec = 1;

              if (parsed > maxSec) {
                parsed = maxSec;
              }
              else if (parsed < minSec) {
                parsed = minSec;
              }
              
              shameTime[guildId] = parsed;
            }
            else if (shameTimeArg.includes(':')) {
              var shameTimeSplit = shameTimeArg.split(':');

              if (shameTimeSplit.length != 2) {
                message.channel.send(
                  `The argument '${shameTimeArg}' for ${shameCommand.parameters[1].name} cannot be parsed.`,
                  {embed: shameHelpSecondArgumentEmbed}
                );
                ResetShameInfo(message.guild.id);
                return;
              }
              else {
                var parsed0 = parseInt(shameTimeSplit[0], 10);
                var parsed1 = parseInt(shameTimeSplit[1], 10);

                if (isNaN(parsed0) || isNaN(parsed1)) {
                  message.channel.send(
                    `The argument '${shameTimeArg}' for ${shameCommand.parameters[1].name} could not be parsed and was recognized as being in the format of MM:SS.`,
                    {embed: shameHelpSecondArgumentEmbed}
                  );
                  ResetShameInfo(message.guild.id);
                  return;
                }

                const maxMin = 1;
                const minMin = 0;
                const maxSec = 30;
                const minSec = 1;
      
                if (parsed0 > maxMin) {
                  parsed0 = maxMin;
                  parsed1 = maxSec;
                }
                else if (parsed0 >= maxMin && parsed1 > maxSec) {
                  parsed0 = maxMin;
                  parsed1 = maxSec;
                }
                else if (parsed0 < minMin) {
                  parsed0 = minMin;
                  parsed1 = minSec;
                }
                else if (parsed0 <= minMin && parsed1 < minSec) {
                  parsed0 = minMin;
                  parsed1 = minSec;
                }

                shameTime[guildId] = parsed0 * 60 + parsed1;
              }
            }
            else {
              message.channel.send(
                `Please retry the ${shameCommand.name} command with the ${shameCommand.parameters[1].name} argument in one of the accepted formats.`,
                {embed: shameHelpSecondArgumentEmbed}
              );
              ResetShameInfo(message.guild.id);
              return;
            }
          }
          else {
            shameTime[guildId] = shameTimeDefault;
          }

          if (parameters.length > 2) {
            const secondsToVoteArg = parameters[2];

            if (!secondsToVoteArg.includes(':')) {
              var parsed = parseInt(secondsToVoteArg, 10);

              if (isNaN(parsed)) {
                message.channel.send(
                  `The argument '${secondsToVoteArg}' for ${shameCommand.parameters[2].name} could not be parsed as a number.`,
                  {embed: shameHelpThirdArgumentEmbed}
                );
                ResetShameInfo(message.guild.id);
                return;
              }

              const maxSec = 90;
              const minSec = 5;

              if (parsed > maxSec) {
                parsed = maxSec;
              }
              else if (parsed < minSec) {
                parsed = minSec;
              }

              secondsToVote[guildId] = parsed;
            }
            else if (secondsToVoteArg.includes(':')) {
              var secondsToVoteOptionalSplit = secondsToVoteArg.split(':');
              if (secondsToVoteOptionalSplit.length != 2) {
                message.channel.send(
                  `The argument '${secondsToVoteArg}' for ${shameCommand.parameters[2].name} cannot be parsed.`,
                  {embed: shameHelpThirdArgumentEmbed}
                );
                ResetShameInfo(message.guild.id);
                return;
              }

              var parsed0 = parseInt(secondsToVoteOptionalSplit[0], 10);
              var parsed1 = parseInt(secondsToVoteOptionalSplit[1], 10);

              if (isNaN(parsed0) || isNaN(parsed1)) {
                message.channel.send(
                  `The argument '${secondsToVoteArg}' for ${shameCommand.parameters[2].name} could not be parsed and was recognized as being in the format of MM:SS.`,
                  {embed: shameHelpThirdArgumentEmbed}
                );
                ResetShameInfo(message.guild.id);
                return;
              }

              const maxMin = 1;
              const minMin = 0;
              const maxSec = 30;
              const minSec = 5;

              if (parsed0 > maxMin) {
                parsed0 = maxMin;
                parsed1 = maxSec;
              }
              else if (parsed0 >= maxMin && parsed1 > maxSec) {
                parsed0 = maxMin;
                parsed1 = maxSec;
              }
              else if (parsed0 < minMin) {
                parsed0 = minMin;
                parsed1 = minSec;
              }
              else if (parsed0 <= minMin && parsed1 < minSec) {
                parsed0 = minMin;
                parsed1 = minSec;
              }

              secondsToVote[guildId] = parsed0 * 60 + parsed1;
            }
            else {
              message.channel.send(
                `Please retry the ${shameCommand.name} command with the ${shameCommand.parameters[2].name} argument in one of the accepted formats.`,
                {embed: shameHelpThirdArgumentEmbed}
              );
              ResetShameInfo(message.guild.id);
              return;
            }
          }
          else {
            secondsToVote[guildId] = secondsToVoteDefault;
          }
        }
        else {
          shameTime[guildId] = shameTimeDefault;
          secondsToVote[guildId] = secondsToVoteDefault;
        }

        var shameTimeSeconds = shameTime[guildId] % 60;
        var shameTimeSecondsStr = shameTimeSeconds.toString();
        if (shameTimeSeconds < 10) {
          shameTimeSecondsStr = `0${shameTimeSeconds}`;
        }
        shameTimeStr[guildId] = `${Math.floor(shameTime[guildId] / 60)}:${shameTimeSecondsStr}`;
        
        shameMessageContent[guildId] = [
          `All in favour of sending ${shameMember[guildId].displayName} to the ${dungeons[guildId]} for ${shameTimeStr[guildId]} react with a 👍, all against reply with 👎`,
          `${secondsToVote[guildId]} seconds left to vote!`
        ];

        message.channel.send(shameMessageContent[guildId])
          .then(message => {
            const guildId = message.guild.id;
            shameMessage[guildId] = message;
            shameMessageTimer[guildId] = setInterval(ShameMessageUpdate, 1000 * shameTimerInterval, guildId);
            message.react('👍')
              .then(messageReaction => {
                messageReaction.message.react('👎');
              });
          })
          .catch(console.error);
      }
    }
    else {
      message.channel.send(
        `The ${shameCommand.name} command requires ${shameCommand.parameters[0].name}`,
        {embed: shameHelpFirstArgumentEmbed}
      );
      return;
    }
  }
  else {
    message.channel.send({embed: shameHelpEmbed});
  }
};

var SetDungeon = async function(message) {
  var allowed = false;
  if (guildRules[message.guild.id][adminSetDungeonRuleName]) {
    if (guildRules[message.guild.id][adminRoleRuleName].length > 0
      && message.guild.member(message.author).roles.has(guildRules[message.guild.id][adminRoleRuleName])) {
      allowed = true;
    }
    else {
      message.channel.send(
        `The rule \`${adminSetDungeonRuleName}\` is set to \`true\` and you don't have the ${message.guild.roles.get(guildRules[message.guild.id][adminRoleRuleName])} role. Please contact an ${message.guild.roles.get(guildRules[message.guild.id][adminRoleRuleName])} to set a dungeon.`
      );
    }
  }
  else {
    allowed = true;
  }

  if (allowed) {
    if (message.content.trimRight().length > setDungeonCommand.name.length) {
      const guildId = message.guild.id;

      if (shameMessage[guildId]) {
        message.channel.send([
          `The dungeon cannot be changed while there is a vote in progress!`,
          `Please wait till the vote initiated by ${shameAuthor[guildId]} against ${shameMember[guildId]} has finished (there is still ${shameTime[guildId]} seconds left).`
        ]);
        return;
      }
      
      const dungeonName = message.content.slice(`${setDungeonCommand.name} `.length);
      var errorReason = `Could not find ${dungeonName} in the channels of the ${message.guild} server!`;
      var dungeon = null;
      var notVoice = false;

      var channel = message.guild.channels.find(function(val) {
        if (val.name === dungeonName) {
          if (val.type === 'voice') {
            return true;
          }

          notVoice = true;
          return false;
        }

        return false;
      });

      if (channel) {
        dungeon = channel;
      }
      else if (notVoice) {
        errorReason = `Found the channel ${dungeonName} but it's not a voice channel!`;
      }

      if (dungeon === null) {
        message.channel.send(
          errorReason,
          {embed: setDungeonHelpEmbed}
        );
        return;
      }
      else {
        if (dungeon === dungeons[guildId]) {
          message.channel.send(`${dungeon} is already the dungeon for ${message.guild}.`);
          return;
        }
        else {
          // Internal cached dungeon
          dungeons[guildId] = dungeon;

          // Update the database for this guild 
          try {
            var dungeonData = {
              id: dungeons[guildId].id
            };
            await dungeonsDb.doc(guildId).set(dungeonData);

            message.channel.send(`Successfully set the dungeon to ${dungeons[guildId]} for ${message.guild}!`);

            voiceConnections[guildId] = await dungeon.join();

            console.log(`Successfully joined ${dungeon.name}!`);
          }
          catch(error) {
            if (errorChannel[guildId]) {
              errorChannel[guildId].send(
                {embed: {
                  title: 'Error',
                  description: `An error occurred while updating the dungeon ${message.guild}.`,
                  color: 14483459,
                  fields: [
                    {
                      name: 'Reason',
                      value: error
                    }
                  ]
                }}
              );
            }
            else {
              console.error(error);
            }
          };
        }
      }
    }
    else {
      message.channel.send({embed: setDungeonHelpEmbed});
    }
  }
};

var ValidateRule = function(guildId, key, value) {
  var result = {
    isValid: false,
    invalidReason: '',
    validObject: null,
    rule: null
  };
  
  result.rule = rules.find(function(value) {
    if (value.name === key) {
      return true;
    }
    return false;
  });

  if (result.rule !== null) {
    switch (result.rule.valueType) {
    case ruleValueTypes[ruleValueTypeIndices.bool]: {
      result.isValid = value === 'true' || value === 'false';
      result.validObject = value === 'true';
      break;
    }
    case ruleValueTypes[ruleValueTypeIndices.role]: {
      var role = client.guilds.get(guildId).roles.find(function(val) {
        return val.name === value;
      });

      if (role) {
        result.isValid = true;
        result.validObject = role;
      }

      break;
    }
    case ruleValueTypes[ruleValueTypeIndices.textChannel]: {
      var channel = client.guilds.get(guildId).channels.find(function(val) {
        return val.name === value && val.type === 'text';
      });

      if (channel) {
        result.isValid = true;
        result.validObject = channel;
      }

      break;
    }
    }
  }
  else {
    result.invalidReason = `Could not find \`${key}\` in ${client.guilds.get(guildId)} rules.`
    return result;
  }

  if (!result.isValid) {
    result.invalidReason = `\`${value}\` is not a valid value for \`${result.rule.name}\`.`
  }

  return result;
};

var SetRule = function(message) {
  var allowed = false;
  if (guildRules[message.guild.id][adminSetRuleRuleName]) {
    if (guildRules[message.guild.id][adminRoleRuleName].length > 0
      && message.guild.member(message.author).roles.has(guildRules[message.guild.id][adminRoleRuleName])) {
      allowed = true;
    }
    else {
      message.channel.send(
        `The rule \`${adminSetRuleRuleName}\` is set to \`true\` and you don't have the ${message.guild.roles.get(guildRules[message.guild.id][adminRoleRuleName])} role. Please contact an ${message.guild.roles.get(guildRules[message.guild.id][adminRoleRuleName])} to set a rule.`
      );
    }
  }
  else {
    allowed = true;
  }

  if (allowed) {
    if (message.content.trimRight().length > setRuleCommand.name.length) {
      var keyValuePair = message.content.trimRight().replace('!setRule ', '');
      var keyValueSplit = keyValuePair.split(':');

      if (keyValueSplit.length != 2) {
        message.channel.send(
          `Could not parse argument '${keyValuePair}' as ${setRuleCommandFirstArgumentName}:${setRuleCommandSecondArgumentName}`,
          {embed: setRuleHelpEmbed}
        );
        return;
      }

      var key = keyValueSplit[0];
      var value = keyValueSplit[1];

      var validateResult = ValidateRule(message.guild.id, key, value);
      if (validateResult.isValid) {
        var actualValue = null;

        switch (validateResult.rule.valueType) {
        case ruleValueTypes[ruleValueTypeIndices.bool]: {
          actualValue = validateResult.validObject;
          break;
        }
        case ruleValueTypes[ruleValueTypeIndices.role]:
        case ruleValueTypes[ruleValueTypeIndices.textChannel]: {
          actualValue = validateResult.validObject.id;
          break;
        }
        }

        if (actualValue != guildRules[message.guild.id][key]) {
          guildRules[message.guild.id][key] = actualValue;

          rulesDb.doc(message.guild.id).set(guildRules[message.guild.id]).then(res => {
            message.channel.send(`Successfully set rule ${key} to ${value}!`);
          }).catch(error => {
            if (errorChannel[message.guild.id]) {
              errorChannel[message.guild.id].send(
                {embed: {
                  title: 'Database Error',
                  description: `Could not update the database for ${message.guild} with the rule ${key}.`,
                  color: 14483459,
                  fields: [
                    {
                      name: 'Reason',
                      value: error
                    }
                  ]
                }}
              );
            }
            else {
              console.error(error);
            }
          });
        }
        else {
          message.channel.send(`Rule \`${key}\` is already set to \`${value}\`.`);
        }
      }
      else {
        message.channel.send(
          validateResult.invalidReason,
          {embed: listRulesHelpEmbed}
        );
      }
    }
    else {
      message.channel.send(
        {embed: setRuleHelpEmbed}
      );
    }
  }
};

var Help = function(message) {
  if (message.content.trimRight().length > helpCommand.name.length) {
    const commandName = message.content.slice(`${helpCommand.name} `.length).replace(/!/g, '');

    if (commandName === 'help') {
      message.channel.send({
        embed: {
          title: `${helpCommand.name} help`,
          color: 1957120,
          image: {
            url: 'https://media.giphy.com/media/3o7aDggJJneFF2Mjks/giphy.gif'
          }
        }
      });
    }
    else if (commandName === setDungeonCommand.name.replace('!', '')) {
      message.channel.send({embed: setDungeonHelpEmbed});
    }
    else if (commandName === shameCommand.name.replace('!', '')) {
      message.channel.send({embed: shameHelpEmbed});
    }
    else if (commandName === setRuleCommand.name.replace('!', '')) {
      message.channel.send({embed: setRuleHelpEmbed});
    }
    else if (commandName === listRules.name.replace('!', '')) {
      message.channel.send({embed: listRulesHelpEmbed});
    }
    else if (commandName === cancelCommand.name.replace('!', '')) {
      message.channel.send({embed: cancelHelpEmbed});
    }
    else if (commandName === rulesCommand.name.replace('!', '')) {
      message.channel.send({embed: rulesHelpEmbed})
    }
    else {
      message.channel.send([
        `I did not recognize ${commandName} as a command...`
      ]);
    }
  }
  else {
    message.channel.send({embed: generalHelpEmbed}).catch(console.error);
  }
};

var Cancel = function(message) {
  if (shameAuthor[message.guild.id]) {
    var author = message.guild.member(message.author);

    if (shameAuthor[message.guild.id].id === author.id) {
      shameMessageContent[message.guild.id][1] = `Canceled by initiator!`;
      shameMessage[message.guild.id].edit(shameMessageContent[message.guild.id]);
      shameMessage[message.guild.id].clearReactions();

      ResetShameInfo(message.guild.id);
      return;
    }
    else if (guildRules[message.guild.id][adminCancelRuleName] 
      && guildRules[message.guild.id][adminRoleRuleName].length > 0 
      && author.roles.has(guildRules[message.guild.id][adminRoleRuleName])) {
      shameMessageContent[message.guild.id][1] = `Canceled by ${message.guild.roles.get(guildRules[message.guild.id][adminRoleRuleName])}!`;
      shameMessage[message.guild.id].edit(shameMessageContent[message.guild.id]);
      shameMessage[message.guild.id].clearReactions();

      ResetShameInfo(message.guild.id);
      return;
    }
    else {
      var messageContent = `Tsk tsk, that's not how this works... You must ask ${author} to cancel the vote.`
      if (guildRules[message.guild.id].adminCancel) {
        messageContent = `Tsk tsk, that's not how this works... You must ask ${author} or someone with ${message.guild.roles.get(guildRules[message.guild.id].adminRole)} role to cancel the vote.`
      }
      message.channel.send(messageContent);
    }
  }
  else {
    message.channel.send({embed: cancelHelpEmbed});
  }
};

var Rules = function (message) {
  var rulesEmbedFields = Object.keys(guildRules[message.guild.id]).map(function(ruleName) {
    var ruleCurrentValue = null;

    for (var i in rules) {
      var rule = rules[i];
      if (rule.name === ruleName) {
        switch (rule.valueType) {
        case ruleValueTypes[ruleValueTypeIndices.bool]: {
          ruleCurrentValue = guildRules[message.guild.id][ruleName];
          break;
        }
        case ruleValueTypes[ruleValueTypeIndices.role]: {
          if (guildRules[message.guild.id][ruleName].length > 0) {
            ruleCurrentValue = message.guild.roles.get(guildRules[message.guild.id][ruleName]);
          }
          else {
            ruleCurrentValue = 'unset';
          }
          break;
        }
        case ruleValueTypes[ruleValueTypeIndices.textChannel]: {
          if (guildRules[message.guild.id][ruleName].length > 0) {
            ruleCurrentValue = message.guild.channels.get(guildRules[message.guild.id][ruleName]);
          }
          else {
            ruleCurrentValue = 'unset';
          }
          break;
        }
        }

        break;
      }
    }

    return {
      name: ruleName,
      value: ruleCurrentValue.toString()
    }
  });

  rulesEmbedFields.sort(function(el1, el2) {
    return el1.name > el2.name;
  });
  rulesEmbed.fields = rulesEmbedFields;

  message.channel.send({embed: rulesEmbed});
}

client.on('message', message => {
  if (!message.guild) return;

  if (message.content.startsWith(shameCommand.name)) {
    Shame(message);
  }
  else if (message.content.startsWith(setDungeonCommand.name)) {
    SetDungeon(message);
  }
  else if (message.content.startsWith(setRuleCommand.name)) {
    SetRule(message);
  }
  else if (message.content.startsWith(helpCommand.name)) {
    Help(message);
  }
  else if (message.content.startsWith(cancelCommand.name)) {
    Cancel(message);
  }
  else if (message.content.startsWith(rulesCommand.name)) {
    Rules(message);
  }
});

client.on('voiceStateUpdate', (oldMember, newMember) => {
  if (!newMember.voiceChannel) {
    return; // They have disconnected from voice
  }
  
  const guildId = oldMember.guild.id;

  if (shamedMembers[guildId] && shamedMembers[guildId][oldMember.id]) {
    if (newMember.voiceChannelID !== dungeons[guildId].id) {
      newMember.setVoiceChannel(dungeons[guildId]);
    }
  }
});

client.on('error', error => {
  console.error(error);
});

client.login('your-bot-token-here')
  .catch(console.error);
